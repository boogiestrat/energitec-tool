<?php

namespace App;

use App\Rapport;
use App\Formulaire;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $appends = [
        'company_id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'compagnie', 'poste', 'operateur_clients', 'is_active', 'is_principal', 'is_rapports', 'adresse', 'ville', 'province', 'code_postal', 'telephone', 'horaire_jours', 'horaire_ouverture', 'horaire_fermeture', 'horaire_feries', 'horaire_mensuel'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getImageAttribute()
    {
        return $this->avatar;
    }

    public function roles() {
        return $this->belongsToMany('App\Role');
    }

    public function formulaires() {
        return $this->hasMany('App\Formulaire', 'client_id', 'company_id');
    }

    public function rapports() {

        return $this->hasMany('App\Rapport', 'client_id', 'company_id')->orderBy('updated_at', 'DESC');
    }

    public function hasAnyRoles($roles) {

        if($this->roles()->whereIn('name', $roles)->first()) {
            return true;
        }

        return false;
    }

    public function hasRole($role) {

        if($this->roles()->where('name', $role)->first()) {
            return true;
        }

        return false;
    }

    public function hasAnyAnalyse($analyse_id)
    {
        foreach($this->formulaires as $formulaire) {
            if($formulaire->hasChamp($analyse_id)) {
                return true;
            }
        }

        return false;
    }

    public function clients()
    {
        $clients = [];
        $users = $this->all();

        foreach($users as $user) {
            if($user->hasRole('client')) {
                $clients[] = $user;
            }
        }

        return $clients;
    }

    public function operateurClients()
    {
        $clients = [];
        $users = json_decode($this->operateur_clients, true);

        if(null !== $users) {
            return $users;
        }

        return $clients;
    }
    
    public function companyId()
    {
        $company = $this->where('compagnie', $this->compagnie)->where('is_principal', 1)->first();

        return $company->id;
    }

    public function getCompanyIdAttribute() {
        $company = $this->where('compagnie', $this->compagnie)->where('is_principal', 1)->first();
        
        if($company) {
            return $company->id;
        }

        return $this->id;
    }
}
