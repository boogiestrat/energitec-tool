<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rapport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'operateur_id',
        'contenu',
        'incomplet',
        'notes'
    ];

    public function formulaire()
    {
        return $this->belongsTo('App\Formulaire');
    }

    public function operateur()
    {
        return $this->belongsTo('\App\User', 'operateur_id');
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function hasAnyAnalyse($analyse_id, $forms)
    {
        foreach($forms as $form) {
            if($form->hasChamp($analyse_id)) {
                return true;
            }
        }
    }

    /**
     * Set the value of the "updated at" attribute.
     *
     * @param  mixed  $value
     * @return void
     */
    public function setUpdatedAt($value)
    {
        $this->{static::UPDATED_AT} = $value;
    }
}
