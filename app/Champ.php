<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Champ extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'formulaire_id',
        'analyse_id',
        'unite',
        'min',
        'max',
        'jaune_min',
        'jaune_max',
        'orange_min',
        'orange_max',
        'rouge_min',
        'rouge_max'
    ];
}
