<?php

namespace App;

use App\Champ;
use Illuminate\Database\Eloquent\Model;

class Formulaire extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'equipement_id',
        'equipement_titre'
    ];

    public function client() {
        return $this->belongsTo('\App\User', 'client_id');
    }

    public function equipement() {
        return $this->belongsTo('App\Equipement');
    }

    public function champs() {
        return $this->hasMany('App\Champ');
    }

    public function champ($analyse_id, $type) {
        $champ = Champ::where(['analyse_id' => $analyse_id, 'formulaire_id' => $this->id])->first();
            if(isset($champ->$type)) {
                return $champ->$type;
            }
    }

    public function hasChamp($analyse_id)
    {
        foreach($this->champs as $champ) {
            if($champ->analyse_id == $analyse_id) {
                return true;
            }
        }

        return false;
    }

    public function getChamp($analyse_id)
    {
        $champ = Champ::where('analyse_id', $analyse_id)->where('formulaire_id', $this->id)->first();

        return $champ;
    }

    /**
     * Get the equipment name.
     *
     * @param  string  $value
     * @return string
     */
    public function getEquipementTitreAttribute($value)
    {
        if('' == $value) {
            $value = $this->equipement->nom;
        }

        /*if(strlen($value) > 16) {
            return str_replace(' ', '<br>', $value);
        }*/

        return $value;
    }
}
