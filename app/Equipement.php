<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipement extends Model
{
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function hasAnalyse($analyse_id)
    {
        foreach($this->analyses as $analyse) {
            if($analyse->id == $analyse_id) {
                return true;
            }
        }

        return false;
    }
}
