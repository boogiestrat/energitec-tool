<?php

namespace App\Http\Requests;

use App\User;
use App\Analyse;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class AnalyseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => ['required', 'min:3'],
            'titre' => ['required', 'min:3'],
            'type' => ['required', 'min:3'],
            'equipement_id' => ['required', 'min:1'],
            'groupe' => ['required', 'min:3']
        ];
    }
}
