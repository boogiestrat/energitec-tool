<?php

namespace App\Http\Controllers;

use PDF;
use App\User;
use App\Rapport;
use App\Champ;
use App\Analyse;
use App\Equipement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RapportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->hasRole('client')) {
            return redirect()->action(
                'RapportsController@list', ['client' => auth()->user()]
            );
        }

        if(auth()->user()->hasRole('operateur')) {
            $operateurClients = json_decode(auth()->user()->operateur_clients);
            $clients = User::whereIn('id', $operateurClients)->get();
        } else {
            $clients = User::whereHas('roles', function($q) {
                $q->where('name', 'client');
            })->get();
        }

        return view('rapports.index')->with([
            'clients' => $clients,
        ]);
    }

    public function list(User $client)
    {
        $rapports = Rapport::where('client_id', $client->id)->latest()->get();

        return view('rapports.list', compact(['rapports', 'client']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $operateur        = auth()->user();
        $analyses         = Analyse::sorted()->get();
        $rapportPrecedent = Rapport::where('client_id', $user->id)->orderBy('created_at', 'desc')->take(1)->first();
        $precedent        = !empty($rapportPrecedent) ? json_decode($rapportPrecedent->contenu) : [];
  
        return view('rapports.create')->with([
            'analyses'  => $analyses,
            'operateur' => $operateur,
            'client'    => $user,
            'precedent' => $precedent,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $results = $request->except(['_token', '_method', 'incomplet', 'notes']);

        // Filter to add colors for hors-normes
        foreach($results as $champ => $valeur) {
            $this->horsNorme($champ, $valeur) ? $results[$champ.'_couleur'] = $this->horsNorme($champ, $valeur) : '';
        }

        $operateur = auth()->user();

        Rapport::create([
            'client_id'    => $user->id,
            'operateur_id' => $operateur->id,
            'contenu'      => json_encode($results),
            'incomplet'    => $request->get('incomplet') ?? 0,
            'notes'        => $request->get('notes')
        ]);

        return redirect()->route('rapports.list', $user)->withStatus(__('Nouveau rapport ajouté.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rapport  $rapport
     * @return \Illuminate\Http\Response
     */
    public function show(Rapport $rapport)
    {
        $analyses = Analyse::sorted()->get();
        $contenu  = json_decode($rapport->contenu);

        return view('rapports.show', compact(['rapport', 'analyses', 'contenu']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rapport  $rapport
     * @return \Illuminate\Http\Response
     */
    public function edit(Rapport $rapport)
    {
        $analyses = Analyse::sorted()->get();
        $contenu  = json_decode($rapport->contenu);

        return view('rapports.edit', compact(['rapport', 'analyses', 'contenu']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rapport  $rapport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rapport $rapport, User $user)
    {
        $results = $request->except(['_token', '_method', 'incomplet', 'notes']);

        $timestamp = true;

        // Filter to add colors for hors-normes
        foreach($results as $champ => $valeur) {
            $this->horsNorme($champ, $valeur) ? $results[$champ.'_couleur'] = $this->horsNorme($champ, $valeur) : '';
        }

        if($results['updated_at'] != null) {
            $rapport->setUpdatedAt(\Carbon\Carbon::parse($results['updated_at'])) ;
            $rapport->timestamps = false;
        }

        $rapport->update([
            'contenu'   => json_encode($results),
            'incomplet' => $request->get('incomplet') ?? 0,
            'notes'     => $request->get('notes')
        ]);

        return redirect()->route('rapports.list', $user)->withStatus(__('NRapport modifié.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rapport  $rapport
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rapport $rapport)
    {
        //
    }

    public function export(Rapport $rapport)
    {
        ini_set('max_execution_time', '300');

        $analyses    = Analyse::sorted()->get();
        $formulaires = $rapport->client->formulaires->groupBy(
            function($item, $key) {
                return Equipement::where('id', $item['equipement_id'])->first()->nom;
            }
        );
        $creation     = new \DateTime($rapport->creation_date);
        $creationDate = $creation->format('Y-m-d');
        $contenu      = json_decode($rapport->contenu);

        //return view('rapports.export', compact(['rapport', 'analyses', 'formulaires', 'contenu']));

        $pdf = PDF::loadView('rapports.export', compact(['rapport', 'analyses', 'formulaires', 'contenu']));

        //eturn $pdf->stream();

        return $pdf->download('rapport' . str_replace(' ', '_', $rapport->client->compagnie) . '_' . $creationDate . '.pdf');

    }

    protected function parseFormulaires()
    {
        $analyses    = Analyse::all();
        $formulaires = $this->formulaires();
    }

    protected function horsNorme($champ, $valeur)
    {
        $couleur = '';

        // Form ID
        $champAsArray = explode('_', $champ);
        $formulaireId = end($champAsArray);
        // Analysis ID
        $analyseName = implode('_', array_slice($champAsArray, 0, -1));
        $analyse = Analyse::where('nom', $analyseName)->first();

        if($analyse) {
            // Field
            $champ = Champ::where(['analyse_id' => $analyse->id, 'formulaire_id' => $formulaireId])->first();

            if($analyse->type == 'text') {
                // Value filters min-max
                $couleur = $this->getHorsNormeColor($champ, $valeur);
            }
        }

        return $couleur;
    }

    protected function getHorsNormeColor($champ, $valeur)
    {
        $colors  = ['', 'jaune', 'orange', 'rouge'];
        $couleur = "";

        foreach($colors as $key => $val) {
            $color  = $val != "" ? $val . "_" : "";
            $newkey = $key < 3 ? $key++ : $key;

            if(isset($champ->{$color . 'min'}) && $champ->{$color . 'min'} != '') {
                if($valeur < $champ->{$color . 'min'}) {
                    $couleur = $colors[$newkey];
                }
            }

            if(isset($champ->{$color . 'max'}) && $champ->{$color . 'max'} != '') {
                if($valeur > $champ->{$color . 'max'}) {
                    $couleur = $colors[$newkey];
                }
            }
        }

        return $couleur;
    }
}
