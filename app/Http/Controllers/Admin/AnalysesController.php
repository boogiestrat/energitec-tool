<?php

namespace App\Http\Controllers\Admin;

use Gate;
use App\Analyse;
use App\Equipement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnalyseRequest;

class AnalysesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $analyses = Analyse::sorted()->get();

        return view('admin.analyses.index', compact('analyses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::denies('edit-users')){
            return redirect()->route('admin.analyses.index');
        }

        $equipements = Equipement::all();

        return view('admin.analyses.create', compact('equipements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Analyse::create($request->all());

        return redirect()->route('admin.analyses.index')->withStatus(__('Nouvelle analyse ajoutée.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Analyse $analysis)
    {
        if(Gate::denies('edit-users')){
            return redirect()->route('admin.analyses.index');
        }

        $analyse = $analysis;
        $equipements = Equipement::all();

        return view('admin.analyses.edit', compact(['analyse', 'equipements']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnalyseRequest $request, Analyse $analysis)
    {
        $analysis->update($request->all());

        return redirect()->route('admin.analyses.index')->withStatus(__('Analyse sauvegardée'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Analyse $analysis)
    {
        $analysis->delete();

        return redirect()->route('admin.analyses.index')->withStatus(__('Analyse supprimée.'));
    }
}
