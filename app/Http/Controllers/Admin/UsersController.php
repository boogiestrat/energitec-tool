<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use Storage;
use App\Equipement;
use Gate;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('auth.admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereHas('roles', function($q) {
            $q->where('name', 'client');
        })->get();

        return view('admin.users.index')->with('users', $users);
    }

    public function list($type)
    {
        $users = User::whereHas('roles', function($q) use($type) {
            $q->where('name', $type);
            if($type == 'client') {
                $q->orWhere('name', 'contact');
            }
        })->orderBy('compagnie', 'ASC')->get();

        $compagnies = $users->groupBy('compagnie');
        $compagnies->toArray();

        return view('admin.users.list')->with([
            'compagnies' => $compagnies,
            'type' => $type
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show')->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        if(Gate::denies('edit-users')){
            return redirect()->route('admin.users.index');
        }

        $roles = Role::all();
        $equipements = Equipement::all();
        $clients = User::whereHas('roles', function($q) {
            $q->where('name', 'client');
        })->get();

        $compagnies = User::distinct('compagnie')->pluck('compagnie');

        return view('admin.users.create')->with([
            'roles' => $roles,
            'equipements' => $equipements,
            'clients' => $clients,
            'compagnies' => $compagnies,
            'type' => $type
        ]);
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $type)
    {
        $request->validate([
            'name'   =>  'required',
            'avatar' =>  'image|mimes:jpeg,png,jpg,gif|max:2048',
            'email' => 'required|email|unique:users',
        ]);

        if ($request->hasFile('avatarFile')) {
                $avatarName = str_replace(' ', '_', $request->name).'_avatar'.time().'.'.$request->file('avatarFile')->extension();
                $request->file('avatarFile')->move(public_path('assets/images/avatars'), $avatarName);
                $request['avatar'] = $avatarName;
        }

        $formData = $request->merge(['password' => Hash::make($request->get('password'))])->all();
        $formData['operateur_clients'] = isset($formData['operateur_clients']) ? json_encode($formData['operateur_clients']) : '';
        $formData['horaire_jours'] = isset($formData['horaire_jours']) ? json_encode($formData['horaire_jours']) : '';
        $formData['horaire_feries'] = isset($formData['horaire_feries']) ? $formData['horaire_feries'] : '0';
        $formData['horaire_mensuel'] = isset($formData['horaire_mensuel']) ? $formData['horaire_mensuel'] : '0';
        $formData['compagnie'] = isset($formData['new-compagnie']) ? $formData['new-compagnie'] : $formData['compagnie'];

        $user = User::create($formData);

        foreach($formData['roles'] as $role) {
            $roleName = Role::where('id', $role)->first();
            $user->roles()->attach($roleName);
        }

        return redirect()->route('admin.users.list', $type)->withStatus(__('Utilisateur ajouté.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if(Gate::denies('edit-users')){
            return redirect()->route('admin.users.index');
        }

        $roles = Role::all();
        $equipements = Equipement::all();
        $clients = User::whereHas('roles', function($q) {
            $q->where('name', 'client');
        })->orderBy('compagnie', 'ASC')->get();
        $compagnies = User::distinct('compagnie')->pluck('compagnie');

        return view('admin.users.edit')->with([
            'user' => $user,
            'roles' => $roles,
            'equipements' => $equipements,
            'clients' => $clients,
            'compagnies' => $compagnies
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name'   =>  'required',
            'avatar' =>  'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if ($request->hasFile('avatarFile')) {
                $avatarName = str_replace(' ', '_', $request->name).'_avatar'.time().'.'.$request->file('avatarFile')->extension();
                $request->file('avatarFile')->move(public_path('assets/images/avatars'), $avatarName);
                $request['avatar'] = $avatarName;
        }

        $hasPassword = $request->get('password');
        $formData = $request->merge(['password' => Hash::make($request->get('password'))])
        ->except([$hasPassword ? '' : 'password']);
        $formData['horaire_jours'] = isset($formData['horaire_jours']) ? json_encode($formData['horaire_jours']) : null;
        $formData['horaire_feries'] = isset($formData['horaire_feries']) ? '1' : '0';
        $formData['horaire_mensuel'] = isset($formData['horaire_mensuel']) ? $formData['horaire_mensuel'] : '0';

        $user->update($formData);
        $user->roles()->sync($request->roles);

        return redirect()->route('admin.users.show', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $type)
    {
        if(Gate::denies('edit-users')){
            return redirect()->route('admin.users.list', $type);
        }

        $user->roles()->detach();
        $user->delete();

        return redirect()->route('admin.users.list', $type);
    }
}
