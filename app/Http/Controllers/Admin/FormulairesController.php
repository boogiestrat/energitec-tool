<?php

namespace App\Http\Controllers\Admin;

use Gate;
use App\User;
use App\Champ;
use App\Analyse;
use App\Formulaire;
use App\Equipement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormulairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formulaires = Formulaire::all();
        return view('admin.formulaires.index')->with('formulaires', $formulaires);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $analyses = Analyse::sorted()->get();
        $equipements = Equipement::all();

        return view('admin.formulaires.create', compact([
            'analyses',
            'user',
            'equipements'
            ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $analyseCount = Analyse::orderBy('id', 'desc')->first();
        $analyses = $request->all();

        $formulaire = Formulaire::create([
            'client_id' => $user->id,
            'equipement_id' => $analyses['equipement_id'],
            'equipement_titre' => $analyses['equipement_titre'] ?? ''
        ]);

        for ($key = 1; $key <= $analyseCount->id; $key++) {
            if(isset($analyses["is_active_$key"])) {
                $unite = isset($analyses["unite_$key"]) ? $analyses["unite_$key"] : '';
                $min = isset($analyses["min_$key"]) ? $analyses["min_$key"] : '';
                $max = isset($analyses["max_$key"]) ? $analyses["max_$key"] : '';
                $jaune_min = isset($analyses["jaune_min_$key"]) ? $analyses["jaune_min_$key"] : '';
                $jaune_max = isset($analyses["jaune_max_$key"]) ? $analyses["jaune_max_$key"] : '';
                $orange_min = isset($analyses["orange_min_$key"]) ? $analyses["orange_min_$key"] : '';
                $orange_max = isset($analyses["orange_max_$key"]) ? $analyses["orange_max_$key"] : '';
                $rouge_min = isset($analyses["rouge_min_$key"]) ? $analyses["rouge_min_$key"] : '';
                $rouge_max = isset($analyses["rouge_max_$key"]) ? $analyses["rouge_max_$key"] : '';
                $result = [
                    'analyse_id' => $key,
                    'formulaire_id' => $formulaire->id,
                    'min' => $min,
                    'max' => $max,
                    'jaune_min' => $jaune_min,
                    'jaune_max' => $jaune_max,
                    'orange_min' => $orange_min,
                    'orange_max' => $orange_max,
                    'rouge_min' => $rouge_min,
                    'rouge_max' => $rouge_max,
                ];
                Champ::create($result);
            }
        }

        return redirect()->route('admin.users.show', $user)->withStatus(__('Nouvel équipement ajouté.'));
    }

    protected function getIdFromKey($key)
    {
        return ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Formulaire  $formulaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Formulaire $formulaire)
    {
        if(Gate::denies('edit-users')){
            return redirect()->route('admin.formulaires.index');
        }

        $analyses = Analyse::sorted()->get();
        $equipements = Equipement::all();

        return view('admin.formulaires.edit', compact('formulaire', 'analyses', 'equipements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Formulaire  $formulaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formulaire $formulaire)
    {
        $analyseCount = Analyse::orderBy('id', 'desc')->first();
        $analyses = $request->all();

        $formulaire->update([
            'client_id' => $formulaire->client->id,
            'equipement_id' => $analyses['equipement_id'],
            'equipement_titre' => $analyses['equipement_titre'] ?? ''
        ]);

        for ($key = 1; $key <= $analyseCount->id; $key++) {
            if(isset($analyses["is_active_$key"]) && $analyses["is_active_$key"] == 1) {
                $result = [
                    'analyse_id' => $key,
                    'formulaire_id' => $formulaire->id,
                    'unite' => isset($analyses["unite_$key"]) ? $analyses["unite_$key"] : '',
                    'min' => isset($analyses["min_$key"]) ? $analyses["min_$key"] : '',
                    'max' => isset($analyses["max_$key"]) ? $analyses["max_$key"] : '',
                    'jaune_min' => isset($analyses["jaune_min_$key"]) ? $analyses["jaune_min_$key"] : '',
                    'jaune_max' => isset($analyses["jaune_max_$key"]) ? $analyses["jaune_max_$key"] : '',
                    'orange_min' => isset($analyses["orange_min_$key"]) ? $analyses["orange_min_$key"] : '',
                    'orange_max' => isset($analyses["orange_max_$key"]) ? $analyses["orange_max_$key"] : '',
                    'rouge_min' => isset($analyses["rouge_min_$key"]) ? $analyses["rouge_min_$key"] : '',
                    'rouge_max' => isset($analyses["rouge_max_$key"]) ? $analyses["rouge_max_$key"] : '',
                ];

                $oldChamp = Champ::where(['analyse_id' => $key, 'formulaire_id' => $formulaire->id])->first();

                if($oldChamp) {
                    $oldChamp->update($result);
                } else {
                    Champ::create($result);
                }
            } else {
                $oldChamp = Champ::where(['analyse_id' => $key, 'formulaire_id' => $formulaire->id])->first();

                if($oldChamp) {
                    $oldChamp->delete();
                }
            }
        }

        return redirect()->route('admin.users.show', $formulaire->client)->withStatus(__('Formulaire sauvegardé'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Formulaire  $formulaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formulaire $formulaire)
    {
        $formulaire->delete();

        return redirect()->route('admin.users.show', $formulaire->client)->withStatus(__('Formulairee supprimé.'));
    }
}
