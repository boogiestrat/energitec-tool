<?php

namespace App\Http\Controllers;

use App\User;
use App\Analyse;
use App\Rapport;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($range='week')
    {
        $range = $this->past($range);
        $data = [];

        // Stats
        if(auth()->user()->hasRole('client')) {
            //$dates = $this->datesArray($range, auth()->user());
            //$series = $this->series($range, auth()->user());

           // $data = ['dates' => $dates, 'series' => $series];
        }

        if(auth()->user()->hasRole('operateur')) {
            $operateurClients = auth()->user()->operateurClients();
            $clients = User::whereIn('id', $operateurClients)->get();
        } else {
            $clients = User::whereHas('roles', function($q) {
                $q->where('name', 'client');
            })->get();
        }

        return view('home')->with($data);
    }

    protected function stats(User $client, $range='week') {
        $dates = $this->datesArray($this->past($range), $client->id);
        $series = $this->series($this->past($range), $client->id);
        $formulaires = $client->formulaires;

        foreach($formulaires as $formulaire) {
            $formulaire->series = $this->equipmentStats($client, $this->past($range), $formulaire->id);
        }

        return view('stats')->with([
            'client' => $client,
            'dates' => $dates, 
            'series' => $series,
            'formulaires' => $formulaires
        ]);
    }

    protected function equipmentStats($client, $past, $formulaireId) {
        $series = [];
        $x = 1;

        //$contenu = $this->contenu($client);
        $analyses = Analyse::sorted()->get();

        $rapports = Rapport::select('contenu')
            ->where('client_id', $client->id)
            ->where('created_at', '>', $past)
            ->get();
        
        if(0 == count($rapports)) {
            $rapports = Rapport::select('contenu')
                ->where('client_id', $client->id)
                ->orderByDesc('created_at')
                ->limit(7)
                ->get();
        }

        foreach($analyses as $analyse) {
            $serie = array();
            $switch = 0;

            foreach($rapports as $rapport) {
                $switch = 0;
                $rapportData = json_decode($rapport->contenu);

                if(isset($rapportData->{$analyse->nom.'_'.$formulaireId}) && is_numeric($rapportData->{$analyse->nom.'_'.$formulaireId})) {
                    $switch = 1;
                    array_push($serie, $rapportData->{$analyse->nom.'_'.$formulaireId}); 
                }
            }
            if(1 == $switch) {
                $series[] = [
                    'id' => $x++,
                    'name' => $analyse->titre,
                    'serie' => $serie
                ];
            }
        }

        return $series;
    }

    protected function past($range) {
        $today = Carbon::now();
        $past = $today->subWeek();
        if('month' == $range) {
            $past = $today->subDays(30);
        }
        if('year' == $range) {
            $past = $today->subDays(365);
        }
        return $past;
    }

    protected function datesArray($past, $client) {
        $datesArray = [];

        $dates = Rapport::select('created_at')
            ->where('client_id', $client)
            ->where('created_at', '>', $past)
            ->get();

        if(0 == count($dates)) {
            $dates = Rapport::select('created_at')
                ->where('client_id', $client)
                ->orderByDesc('created_at')
                ->limit(7)
                ->get();
        }

        foreach($dates as $date) {
            $datesArray[] = date('d/m', strtotime($date->created_at));
        }

        return $datesArray;
    }

    protected function contenu($client) {
        $contenu = Rapport::select('contenu')
            ->where('client_id', $client)
            ->orderByDesc('created_at')
            ->first();

        if(null !== $contenu) {
            return json_decode($contenu->contenu);
        }
        
        return [];
    }

    protected function series($past, $client) {
        $series = [];

        $contenu = $this->contenu($client);

        $rapports = Rapport::select('contenu')
            ->where('client_id', $client)
            ->where('created_at', '>', $past)
            ->get();
        
        if(0 == count($rapports)) {
            $rapports = Rapport::select('contenu')
                ->where('client_id', $client)
                ->orderByDesc('created_at')
                ->limit(7)
                ->get();
        }

        foreach($contenu as $k => $v) {
            $serie = array();

            foreach($rapports as $rapport) {
                $switch = 0;
                $rapportData = json_decode($rapport->contenu);

                $value = $rapportData->$k ?? null;
                if(null != $value && is_numeric($value)) {
                    $switch = 1;
                }
                array_push($serie, $value);  
            }
            if(1 == $switch) {
                $name = str_replace("_comment", "", $k);
                $numericName = substr($name, 0, -2);
                $name = is_numeric($numericName);
                if(is_numeric($numericName)) {
                    $name = substr($name, 0, -2);
                } else {
                    $name = substr($name, 0, -1);
                }
                $kname = Analyse::where('nom', $name)->first();
                $series[] = [
                    'name' => $kname->titre,
                    'serie' => $serie
                ];
            }
        }

        return $series;
    }
}
