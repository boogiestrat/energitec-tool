<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analyse extends Model
{
    use \Rutorika\Sortable\SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom',
        'titre',
        'type',
        'equipement_id',
        'groupe'
    ];

}
