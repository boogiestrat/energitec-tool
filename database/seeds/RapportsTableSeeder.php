<?php

use App\Rapport;
use Illuminate\Database\Seeder;

class RapportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rapport::truncate();

        $rapport = Rapport::create([
            'client_id' => 3,
            'operateur_id' => 2,
            'contenu' => ''
        ]);
    }
}
