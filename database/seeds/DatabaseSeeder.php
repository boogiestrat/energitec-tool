<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([RolesTableSeeder::class]);
        $this->call([UsersTableSeeder::class]);
        $this->call([EquipementsTableSeeder::class]);
        $this->call([AnalysesTableSeeder::class]);
        $this->call([FormulairesTableSeeder::class]);
        $this->call([ChampsTableSeeder::class]);
        $this->call([RapportsTableSeeder::class]);
    }
}
