<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $operateurRole = Role::where('name', 'operateur')->first();
        $clientRole = Role::where('name', 'client')->first();

        $admin = User::create([
                'name' => 'Denis Ricard',
                'email' => 'denis@denisricard.com',
                'email_verified_at' => now(),
                'password' => Hash::make('passpass'),
                'compagnie' => 'Energitec',
                'telephone' => '514-249-1301',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $daniel = User::create([
                'name' => 'Daniel Fredette',
                'email' => 'daniel@energitec.com',
                'email_verified_at' => now(),
                'password' => Hash::make('12345'),
                'compagnie' => 'Energitec',
                'telephone' => '514-249-1301',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $client = User::create([
                'name' => 'Cli Ent',
                'email' => 'boogiestrat@gmail.com',
                'email_verified_at' => now(),
                'password' => Hash::make('passpass'),
                'compagnie' => 'Compagnie 1',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $alex = User::create([
                'name' => 'Alexandre Fredette',
                'email' => 'a.fredette@energitec.com',
                'email_verified_at' => now(),
                'password' => Hash::make('12345'),
                'compagnie' => 'Energitec',
                'telephone' => '514-914-1522',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $steeven = User::create([
                'name' => 'Steeven Antonelli',
                'email' => 's.antonelli@energitec.com',
                'email_verified_at' => now(),
                'password' => Hash::make('12345'),
                'compagnie' => 'Energitec',
                'telephone' => '514-297-6668 ',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $chris = User::create([
                'name' => 'Christopher Blasco',
                'email' => 'chrisblasco001@gmail.com',
                'email_verified_at' => now(),
                'password' => Hash::make('12345'),
                'compagnie' => 'Energitec',
                'telephone' => '514-576-7153',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $farid = User::create([
                'name' => 'Farid Berrouche',
                'email' => 'faridberrouche@yahoo.ca',
                'email_verified_at' => now(),
                'password' => Hash::make('12345'),
                'compagnie' => 'Energitec',
                'telephone' => '514-452-5416 ',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $rabah = User::create([
                'name' => 'Rabah Madour',
                'email' => 'rabah@energitec.com',
                'email_verified_at' => now(),
                'password' => Hash::make('12345'),
                'compagnie' => 'Energitec',
                'telephone' => '514-247-0889',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $jp = User::create([
                'name' => 'Jean-Paul St-Amant',
                'email' => 'jean_p_1958@hotmail.com',
                'email_verified_at' => now(),
                'password' => Hash::make('12345'),
                'compagnie' => 'Energitec',
                'telephone' => '514-667-9566',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $jc = User::create([
                'name' => 'Jean-Claude DiBattista',
                'email' => 'jcdb_1955@live.ca',
                'email_verified_at' => now(),
                'password' => Hash::make('12345'),
                'compagnie' => 'Energitec',
                'telephone' => '438-392-7570',
                'created_at' => now(),
                'updated_at' => now()
            ]);

        $admin->roles()->attach($adminRole);
        $daniel->roles()->attach($operateurRole);
        $daniel->roles()->attach($adminRole);
        $alex->roles()->attach($operateurRole);
        $alex->roles()->attach($adminRole);
        $steeven->roles()->attach($operateurRole);
        $chris->roles()->attach($operateurRole);
        $farid->roles()->attach($operateurRole);
        $rabah->roles()->attach($operateurRole);
        $jp->roles()->attach($operateurRole);
        $jc->roles()->attach($operateurRole);
        $client->roles()->attach($clientRole);
    }
}
