<?php

use App\ProduitChimique;
use Illuminate\Database\Seeder;

class ProduitsChimiquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProduitChimique::truncate();

        ProduitChimique::create(['nom' => 'Ecosteam 06']);
        ProduitChimique::create(['nom' => 'Ecosteam 24']);
        ProduitChimique::create(['nom' => 'Ecoloop']);
        ProduitChimique::create(['nom' => 'Ecotower 02']);
        ProduitChimique::create(['nom' => 'Ecotower 45']);
        ProduitChimique::create(['nom' => 'Ecotower 21']);
        ProduitChimique::create(['nom' => 'Kelox 1040i']);
        ProduitChimique::create(['nom' => 'Klenzamine FC']);
        ProduitChimique::create(['nom' => 'Klenzamine XF']);
        ProduitChimique::create(['nom' => 'Klensol C-22']);
        ProduitChimique::create(['nom' => 'Lokor 2000TM']);
        ProduitChimique::create(['nom' => 'Algeacide-C']);
        ProduitChimique::create(['nom' => 'Ecosperse']);
        ProduitChimique::create(['nom' => 'Isoklenz KT']);
        ProduitChimique::create(['nom' => 'Klencide GA15v']);
        ProduitChimique::create(['nom' => 'Handipak 920']);
        ProduitChimique::create(['nom' => 'Handipak 140']);
        ProduitChimique::create(['nom' => 'Handipak 55']);
        ProduitChimique::create(['nom' => 'Kem-Aqua 11800']);
        ProduitChimique::create(['nom' => 'Biobrom C-100G']);
        ProduitChimique::create(['nom' => 'Towerechlor 56']);
    }
}
