<?php

use App\EquipementUser;
use Illuminate\Database\Seeder;

class EquipementUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EquipementUser::truncate();

        EquipementUser::create([
            'equipement_id' => 1,
            'user_id' => 3
        ]);
    }
}
