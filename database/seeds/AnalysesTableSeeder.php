<?php

use App\Analyse;
use Illuminate\Database\Seeder;

class AnalysesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Analyse::truncate();
        Analyse::create(['nom' => 'etat', 'titre' => 'État', 'type' => 'select', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'psig', 'titre' => 'PSIG', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'compteur_eau', 'titre' => "Compteur d'eau", 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'niveau', 'titre' => 'Niveau', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'alarme_bas_niveau', 'titre' => 'Alarme Bas niveau', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'purge_fond_secondes', 'titre' => 'Purge Fond (sec)', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'controleur', 'titre' => 'Contrôleur', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'pompe_doseuse', 'titre' => 'Pompe doseuse', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'conductivite', 'titre' => 'Conductivité', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'ph', 'titre' => 'PH', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'temperature', 'titre' => 'Température', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'durete', 'titre' => 'Dureté', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'excedent', 'titre' => 'Excédent', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'purges', 'titre' => 'Purges', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'alcalinite_p', 'titre' => 'Alcalinité P', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'alcalinite_t', 'titre' => 'Alcalinité T', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'polymeres', 'titre' => 'Polymères', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'chlore_libre', 'titre' => 'Chlore libre', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'moblybdate', 'titre' => 'Moblybdate', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'atp_free', 'titre' => 'ATP free', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'atp_living', 'titre' => 'ATP living', 'type' => 'text', 'equipement_id' => '0', 'groupe' => 'materiel',]);
        Analyse::create(['nom' => 'ecosteam24', 'titre' => 'Ecosteam 24', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'ecoloop', 'titre' => 'Ecoloop', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'ecotower02', 'titre' => 'Ecotower 02', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'ecotower45', 'titre' => 'Ecotower 45', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'kelox1040i', 'titre' => 'Kelox 1040i', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'klenzamine_fc', 'titre' => 'Klenzamine FC', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'klenzamine_xf', 'titre' => 'Klenzamine XF', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'klensol_c22', 'titre' => 'Klensol C-22', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'algeacide_c', 'titre' => 'Algeacide-C', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'isoklenz_kt', 'titre' => 'Isoklenz KT', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'klencide_ga15v', 'titre' => 'Klencide GA15v', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'handipak920', 'titre' => 'Handipak 920', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'handipak140', 'titre' => 'Handipak 140', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'handipak55', 'titre' => 'Handipak 55', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'kem_aqua11800', 'titre' => 'Kem-Aqua 11800', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'biobrom_c100g', 'titre' => 'Biobrom C-100G', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);
        Analyse::create(['nom' => 'towerchlor56', 'titre' => 'Towerchlor 56', 'type' => 'checkbox', 'equipement_id' => '0', 'groupe' => 'chimique',]);

    }
}
