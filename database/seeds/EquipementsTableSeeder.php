<?php

use App\Equipement;
use Illuminate\Database\Seeder;

class EquipementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Equipement::truncate();

        Equipement::create(['nom' => 'Boiler']);
        Equipement::create(['nom' => "Réservoir d'alimentation"]);
        Equipement::create(['nom' => 'Adoucisseur']);
        Equipement::create(['nom' => 'Condensat']);
        Equipement::create(['nom' => "Tour d'eau"]);
        Equipement::create(['nom' => 'Réseau eau chaude']);
        Equipement::create(['nom' => 'Réseau eau froide']);
    }
}
