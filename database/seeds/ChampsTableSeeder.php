<?php

use App\Champ;
use Illuminate\Database\Seeder;

class ChampsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Champ::truncate();

        Champ::create(['analyse_id' => 1, 'formulaire_id' => 1, 'min' => '', 'max' => '']);
        Champ::create(['analyse_id' => 2, 'formulaire_id' => 1, 'min' => '', 'max' => '']);
        Champ::create(['analyse_id' => 3, 'formulaire_id' => 1, 'min' => '500', 'max' => '600']);
    }
}
