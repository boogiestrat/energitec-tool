<?php

use App\Formulaire;
use Illuminate\Database\Seeder;

class FormulairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Formulaire::truncate();

        Formulaire::create(['client_id' => 3, 'equipement_id' => 1]);
    }
}
