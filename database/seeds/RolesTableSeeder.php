<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'operateur']);
        Role::create(['name' => 'client']);
        Role::create(['name' => 'contact']);
        Role::create(['name' => 'operateur-client']);
        Role::create(['name' => 'operateur-limité']);
    }
}
