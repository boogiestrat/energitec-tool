<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('analyse_id');
            $table->integer('formulaire_id');
            $table->string('unite')->nullable();
            $table->string('min')->nullable();
            $table->string('max')->nullable();
            $table->string('jaune_min')->nullable();
            $table->string('jaune_max')->nullable();
            $table->string('orange_min')->nullable();
            $table->string('orange_max')->nullable();
            $table->string('rouge_min')->nullable();
            $table->string('rouge_max')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champs');
    }
}
