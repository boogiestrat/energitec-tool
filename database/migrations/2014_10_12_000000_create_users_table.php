<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('email', 100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 100);
            $table->string('avatar', 100)->default('avatar.jpg');
            $table->string('compagnie', 100)->nullable();
            $table->string('poste', 100)->nullable();
            $table->text('operateur_clients')->nullable();
            $table->boolean('is_active')->default(1);
            $table->boolean('is_principal')->default(0);
            $table->boolean('is_rapports')->default(0);
            $table->string('adresse', 100)->nullable();
            $table->string('ville', 100)->nullable();
            $table->string('province', 2)->nullable()->default('QC');
            $table->string('code_postal', 20)->nullable();
            $table->string('telephone', 50)->nullable();
            $table->string('horaire_jours', 100)->nullable();
            $table->time('horaire_ouverture')->nullable();
            $table->time('horaire_fermeture')->nullable();
            $table->boolean('horaire_feries')->default(0);
            $table->boolean('horaire_mensuel')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
