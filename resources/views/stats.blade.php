@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
  <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div style="text-align:right;margin-top:1rem;">
                <a href="{{ route('stats', [$client, 'week']) }}" class="btn btn-primary">Semaine</a> 
                <a href="{{ route('stats', [$client, 'month']) }}" class="btn btn-primary">Mois</a> 
                <a href="{{ route('stats', [$client, 'year']) }}" class="btn btn-primary">Année</a>
                </div>
                <div class="card card-nav-tabs card-plain">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Statistiques pour {{ $client->compagnie }}</h4>
                        <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                        
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    @foreach($formulaires as $formulaire)
                                    <li class="nav-item">
                                        <a class="nav-link{{ $loop->first ? ' active' : '' }}" href="#form-{{ $formulaire->id }}" data-toggle="tab" style="font-size:1rem;">{!! $formulaire->equipement_titre !!}</a>
                                    </li>
                                    @endForeach
                                </ul>
                            </div>
                        </div>
                    </div><div class="card-body ">
                        <div class="tab-content text-center">
                            @foreach($formulaires as $formulaire)
                            <div class="tab-pane {{ $loop->first ? ' active' : '' }}" id="form-{{ $formulaire->id }}">
                                @foreach($formulaire->series as $field)
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="card card-chart">
                                        <div class="card-header card-header-success">
                                        </div>
                                        <div class="card-body">
                                        <h4 class="card-title">{{ $field['name'] }}</h4>
                                        <div class="ct-chart ct-golden-section" id="serie-{{  $formulaire->id  }}-{{ $field['id'] }}"></div>
                                        </div>
                                        <div class="card-footer">
                                        <div class="stats">
                                            
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                @endforeach      
                            </div>
                            @endForeach
                        </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>
  </div>
@endsection

@push('scripts')

<script src="/assets/bundles/chartist.bundle.js"></script>
  <script>
    $(document).ready(function() {

      var formulaires = @json($formulaires);

      formulaires.forEach(function(formulaire) {
          formulaire.series.forEach(function(serie) {
                new Chartist.Bar('#serie-'+formulaire.id+'-'+serie.id, {
                    labels: @json( $dates ),
                    series: [serie.serie],
                    
                },
                {
                    low: 0,
                    height: '260px',
                    divisor: serie.divisor
                });
            });
      });

      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $(e.currentTarget.hash).find('.ct-chart').each(function(el, tab) {
            tab.__chartist__.update();
            });
            
      });
        
    });
  </script>
  
@endpush
@push('styles')
<style>
.ct-series-a .ct-bar {
    stroke: #5294d0;
}
span.ct-label.ct-start.ct-vertical {
    white-space: nowrap;
    position: relative;
    top: 15px;
}
</style>
@endpush