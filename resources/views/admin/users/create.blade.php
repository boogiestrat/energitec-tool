@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Users')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ajouter un client</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ dd($error) }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.users.store', $type) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <h3>Type</h3>
                        @foreach($roles as $role)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="roles[]" value="{{ $role->id }}" {{ $role->name == $type ? 'checked="checked"' : '' }}>
                                <label class="form-check-label">{{ $role->name }}</label>
                            </div>
                        @endforeach
                        <hr>
                        <h3>Détails</h3>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Nom</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Courriel</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="password">Mot de passe</label>
                                <input type="password" class="form-control" id="password"  name="password">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="confirm-password">Confirmer le mot de passe</label>
                                <input type="password" class="form-control" id="confirm-password" name="confirm_password">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="avatarFile">Photo</label>
                                <input type="file" class="form-control-file" name="avatarFile" id="avatarFile">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="is_principal" value="1">
                                    <label class="form-check-label">Contact principal</label>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="is_rapports" value="1">
                                    <label class="form-check-label">Reçoit les rapports</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="compagnie">Compagnie</label>
                                <select id="compagnie" name="compagnie" class="form-control">
                                @foreach($compagnies as $compagnie)
                                    <option value="{{ $compagnie }}">{{ $compagnie }}</option>
                                @endForeach
                                </select>
                                 Nouvelle compagnie: <input type="text" class="form-control" id="new-compagnie" name="new-compagnie">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="poste">Poste</label>
                                <input type="text" class="form-control" id="poste"  name="poste" name="poste">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="adresse">Adresse</label>
                                <input type="text" class="form-control" id="adresse" name="adresse">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ville">Ville</label>
                                <input type="text" class="form-control" id="ville" name="ville">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="province">Province</label>
                                <select id="province" name="province" class="form-control">
                                    <option value="QC">QC</option>
                                    <option value="ON">ON</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="code_postal">Code postal</label>
                                <input type="text" class="form-control" id="code_postal"  name="code_postal">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="telephone">Téléphone</label>
                                <input type="text" class="form-control" id="telephone" name="telephone">
                            </div>
                        </div>
                        @if(auth()->user()->hasRole('admin') )
                        <hr>
                        <h3>Clients</h3>
                        <div class="row">
                        @foreach($clients as $client)
                        <div class="form-check col-md-6">
                            <input class="form-check-input" type="checkbox" name="operateur_clients[]" value="{{ $client->id }}">
                            <label class="form-check-label">{{ $client->compagnie }}</label>
                        </div>
                        @endforeach
                        </div>
                        @endif
                        <hr>
                        <h3>Entreprise</h3>
                        <h6>Jours ouvrables</h6>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="lundi">
                            <label class="form-check-label">Lundi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="mardi">
                            <label class="form-check-label">Mardi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="mercredi">
                            <label class="form-check-label">Mercredi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="jeudi">
                            <label class="form-check-label">Jeudi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="vendredi">
                            <label class="form-check-label">Vendredi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="samedi">
                            <label class="form-check-label">Samedi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="dimanche">
                            <label class="form-check-label">Dimanche</label>
                        </div>
                        <div class="form-row">
                            <div class="input-group date col-md-6" id="ouverturetimepicker" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#ouverturetimepicker" id="horaire_ouverture" name="horaire_ouverture">
                                <div class="input-group-append" data-target="#ouverturetimepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                </div>
                            </div>
                            <div class="input-group date col-md-6" id="fermeturetimepicker" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#fermeturetimepicker" name="horaire_fermeture" />
                                <div class="input-group-append" data-target="#fermeturetimepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="custom-control custom-switch col-md-6 text-center">
                                <input type="checkbox" class="custom-control-input" id="horaire_feries" name="horaire_feries" value="1">
                                <label class="custom-control-label" for="horaire_feries">Jours fériés</label>
                            </div>
                            <div class="custom-control custom-switch col-md-6 text-center">
                                <input type="checkbox" class="custom-control-input" id="horaire_mensuel" name="horaire_mensuel" value="1">
                                <label class="custom-control-label" for="horaire_mensuel">Visite mensuelle</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
  <script>
    $(document).ready(function(){
            $('#ouverturetimepicker').datetimepicker({
                format: 'LT'
            });
            $('#fermeturetimepicker').datetimepicker({
                format: 'LT'
            });
        });
  </script>
@endpush
