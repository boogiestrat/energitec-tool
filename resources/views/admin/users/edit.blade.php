@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Users')])

@push('styles')
<style>
    .operateur-clients {
        display:flex;
        flex-direction:row;
        flex-wrap: wrap;
        align-content: stretch;
    }
    @media (min-width:768px) {
        .operateur-clients > div {
            width:48%;
        }
    }
</style>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Édition de l'utilisateur {{ $user->name }}</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.users.update', $user) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <h3>Type</h3>
                        @foreach($roles as $role)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="roles[]" value="{{ $role->id }}" {{ $user->hasRole($role->name) ? 'checked=checked' :'' }}>
                                <label class="form-check-label">{{ $role->name }}</label>
                            </div>
                        @endforeach
                        <hr>
                        <h3>Détails</h3>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Nom</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Courriel</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="password">Mot de passe</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="confirm-password">Confirmer le mot de passe</label>
                                <input type="password" class="form-control" id="confirm-password" name="confirm-password">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-9">
                                <label for="avatarFile">Photo</label>
                                <input type="file" class="form-control-file" name="avatarFile" id="avatarFile">
                            </div>
                            <div class="form-group col-md-3">
                                <img src="/assets/images/avatars/{{ $user->avatar }}" class="user-photo" style="max-height:40px;border:solid 1px #333;">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="is_principal" value="1" {{ $user->is_principal == 1 ? 'checked="checked"' : '' }}>
                                    <label class="form-check-label">Contact principal</label>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="is_rapports" value="1" {{ $user->is_rapports == 1 ? 'checked="checked"' : '' }}>
                                    <label class="form-check-label">Reçoit les rapports</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="compagnie">Compagnie</label>
                                <select id="compagnie" name="compagnie" class="form-control">
                                @foreach($compagnies as $compagnie)
                                    <option value="{{ $compagnie }}" {{ $user->compagnie == $compagnie ? ' selected' : '' }}>{{ $compagnie }}</option>
                                @endForeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="poste">Poste</label>
                                <input type="text" class="form-control" id="poste" name="poste" value="{{ $user->poste }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="adresse">Adresse</label>
                                <input type="text" class="form-control" id="adresse" name="adresse" value="{{ $user->adresse }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ville">Ville</label>
                                <input type="text" class="form-control" id="ville" name="ville" value="{{ $user->ville }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="province">Province</label>
                                <select id="province" name="province" class="form-control">
                                    <option value="QC">QC</option>
                                    <option value="ON">ON</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="code_postal">Code postal</label>
                                <input type="text" class="form-control" id="code_postal" name="code_postal" value="{{ $user->code_postal }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="telephone">Téléphone</label>
                                <input type="text" class="form-control" id="telephone" name="telephone" value="{{ $user->telephone }}">
                            </div>
                        </div>
                        <hr>
                        @if($user->hasRole('operateur') && !$user->hasRole('admin') && auth()->user()->hasRole('admin') )
                        <h3>Clients</h3>
                        <div class="row">
                        @foreach($clients as $client)
                        <div class="form-check col-md-6">
                            <input class="form-check-input" type="checkbox" name="operateur_clients[]" value="{{ $client->id }}" {{ in_array($client->id, $user->operateurClients()) ? 'checked="checked"' : '' }}>
                            <label class="form-check-label">{{ $client->compagnie }}</label>
                        </div>
                        @endforeach
                        </div>
                        @endif
                        @if($user->hasRole('client'))
                        <h3>Entreprise</h3>
                        <h6>Jours ouvrables</h6>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="lundi">
                            <label class="form-check-label">Lundi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="mardi">
                            <label class="form-check-label">Mardi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="mercredi">
                            <label class="form-check-label">Mercredi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="jeudi">
                            <label class="form-check-label">Jeudi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="vendredi">
                            <label class="form-check-label">Vendredi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="samedi">
                            <label class="form-check-label">Samedi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="horaire_jours[]" value="dimanche">
                            <label class="form-check-label">Dimanche</label>
                        </div>
                        <div class="form-row">
                            <div class="input-group date col-md-6" id="ouverturetimepicker" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#ouverturetimepicker" id="horaire_ouverture">
                                <div class="input-group-append" data-target="#ouverturetimepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                </div>
                            </div>
                            <div class="input-group date col-md-6" id="fermeturetimepicker" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#fermeturetimepicker" />
                                <div class="input-group-append" data-target="#fermeturetimepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="custom-control custom-switch col-md-6">
                                <input type="checkbox" class="custom-control-input" id="horaire_feries" name="horaire_feries" {{ $user->horaire_feries == 1 ? 'checked="checked"' : '' }}>
                                <label class="custom-control-label" for="horaire_feries">Jours fériés</label>
                            </div>
                            <div class="custom-control custom-switch col-md-6">
                                <input type="checkbox" class="custom-control-input" id="horaire_mensuel" name="horaire_mensuel" {{ $user->horaire_mensuel == 1 ? 'checked="checked"' : '' }}>
                                <label class="custom-control-label" for="horaire_mensuel">Visite mensuelle</label>
                            </div>
                        </div>
                        @endif
                         <hr>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
  <script>
    $(document).ready(function(){
            $('#ouverturetimepicker').datetimepicker({
                format: 'LT'
            });
            $('#fermeturetimepicker').datetimepicker({
                format: 'LT'
            });
        });
  </script>
@endpush
