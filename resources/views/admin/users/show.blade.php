@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Users')])

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card social">
                <div class="profile-header d-flex justify-content-between justify-content-center">
                    <div class="d-flex">
                        <div class="details">
                            <h3 class="mb-0">{{ $user->compagnie }}</h3>
                            <span class="text-light">
                                {!! $user->adresse ? $user->adresse . '<br>' : '' !!}
                                {!! $user->ville ? $user->ville . ', ' . $user->province . '<br>' : '' !!}
                                {!! $user->code_postal ? $user->code_postal . '<br>' : '' !!}
                            </span>
                            @if($user->avatar != 'avatar.jpg')
                            <img src="/assets/images/avatars/{{ $user->avatar }}" class="user-photo" alt="{{ $user->name }}" style="float:left;margin:0 5px 0 0;height:40px; width:40px;object-fit: cover;border-radius: 4px;">
                            @endif
                            <p class="mb-0">
                                <span>Contact: <strong>{{ $user->name }}</strong> {{ $user->poste }}</span><br>
                                <span>Tel: <strong>{{ $user->telephone }}</strong></span><br>
                                <span>Email: <strong>{{ $user->email }}</strong></span><br>
                            </p>
                        </div>
                    </div>
                    <div>
                        <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-link float-right"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            </div>
            @if($user->hasRole('client') || $user->hasRole('contact'))
            <a href="{{ route('stats', $user) }}" class="btn btn-primary">Statistiques</a>
            <div class="card">
                <div class="card-header">Équipements <a href="{{ route('admin.formulaires.create', $user) }}" class="btn btn-primary float-right"><i class="fa fa-plus"></i></a></div>
                <div class="card-body">
                    <table class="table">
                        <tbody>
                        @foreach($user->formulaires as $formulaire)
                            <tr>
                            <th scope="row">{!! $formulaire->equipement_titre !!}</th>
                            <td>
                                <a href="{{ route('admin.formulaires.edit', $formulaire) }}"><button type="button" class="btn btn-link float-left"><i class="fa fa-pencil"></i></button></a>
                                <form action="{{ route('admin.formulaires.destroy', $formulaire) }}" method="POST" class="float-left">
                                    @csrf
                                    @method('delete')
                                    <button type="button" class="btn btn-link" onclick="confirm('{{ __("Êtes-vous certain de vouloir supprimer cet équipement?") }}') ? this.parentElement.submit() : ''">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </form>
                            </td>
                            </tr>
                        @endForeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Rapports <a href="{{ route('rapports.create', $user) }}" class="btn btn-primary float-right"><i class="fa fa-plus"></i></a></div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Opérateur</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($user->rapports as $rapport)
                            <tr>
                            <td>{{ $rapport->updated_at }}</th>
                            <td>{{ $rapport->operateur->name }}</th>
                            <td>
                                <a href="{{ route('rapports.show', $rapport) }}"><button type="button" class="btn btn-link float-left"><i class="fa fa-eye"></i></button></a>
                                <a href="{{ route('rapports.export', $rapport) }}"><button type="button" class="btn btn-link float-left"><i class="fa fa-file-pdf-o"></i></button></a>
                                <a href="{{ route('rapports.edit', $rapport) }}"><button type="button" class="btn btn-link"><i class="fa fa-pencil"></i></button></a>
                            </td>
                            </tr>
                        @endForeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
        </div>
    </div>

@endsection
