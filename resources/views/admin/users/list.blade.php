@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Users')])

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  {{ ucfirst($type) }}s 
                  <a href="{{ route('admin.users.create', $type) }}" class="btn btn-link float-right">
                    <i class="fa fa-plus"></i>
                  </a>
                </div>
                <div class="card-body" style="overflow: auto;padding:0;">
                    <table class="table table-striped table-responsive-sm table-rapport">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">Compagnie</th>
                          <th scope="col">Nom</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($compagnies as $k => $v)
                        <tr>
                          <td>{{ $k }}</td>
                          <td style="text-left">
                            @foreach($v as $user)
                            <div class="d-flex justify-content-between">
                              <div style="line-height: 30px;">
                                {{ $user->name }}
                              </div>
                              <div class="user-action d-flex justify-content-end" style="min-width:155px;">
                                @if($user->is_principal == 1)
                                  <button type="button" class="btn btn-link"><i class="fa fa-id-badge"></i></button>
                                @endif
                                <form action="{{ route('admin.users.destroy', [$user, $type]) }}" method="POST" style="display:inline-flex;">
                                    @csrf
                                    @method('post')
                                    <button type="submit" class="btn btn-link"><i class="fa fa-trash-o"></i></button>
                                </form>
                                <a href="{{ route('admin.users.edit', $user) }}">
                                  <button type="button" class="btn btn-link"><i class="fa fa-pencil"></i></button>
                                </a>
                                <a href="{{ route('admin.users.show', $user) }}">
                                  <button type="button" class="btn btn-link"><i class="fa fa-eye"></i></button>
                                </a>
                                <a href="{{ route('stats', $user) }}">
                                  <button type="button" class="btn btn-link"><i class="fas fa-circle"></i></button>
                                </a>
                              </div>
                            </div>
                            @endForeach
                          </td>
                        </tr>
                      @endForeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
