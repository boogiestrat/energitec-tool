@extends('layouts.app', ['activePage' => 'analyses', 'titlePage' => __('Analyses')])

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Analyses <a href="{{ route('admin.analyses.create') }}" class="btn btn-primary float-right">Ajouter</a></div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Ordre</th>
                                <th scope="col">Titre</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Type</th>
                                <th scope="col">Groupe</th>
                            </tr>
                        </thead>
                        <tbody class="sortable" data-entityname="analyse">
                        @foreach($analyses as $analyse)
                            <tr data-itemId="{{{ $analyse->id }}}">
                                <td class="sortable-handle"><i class="fa fa-sort"></i></td>
                                <td class="id-column" style="display:none;">{{ $analyse->id }}</td>
                                <th scope="row">{{ $analyse->titre }}</td>
                                <td>{{ $analyse->nom }}</th>
                                <td>{{ $analyse->type }}</td>
                                <td>{{ $analyse->groupe }}</td>
                                <td>
                                    <a href="{{ route('admin.analyses.edit', $analyse) }}"><button type="button" class="btn btn-link"><i class="fa fa-pencil"></i></button></a>
                                    <form action="{{ route('admin.analyses.destroy', $analyse) }}" method="POST" class="float-left">
                                        @csrf
                                        @method('delete')
                                        <button type="button" class="btn btn-link" onclick="confirm('{{ __("Êtes-vous certain de vouloir supprimer cette analyse?") }}') ? this.parentElement.submit() : ''"><i class="fa fa-trash-o"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endForeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /**
     *
     * @param type string 'insertAfter' or 'insertBefore'
     * @param entityName
     * @param id
     * @param positionId
     */
     var changePosition = function(requestData){
        $.ajax({
            'url': '/admin/analyses/sort',
            'type': 'POST',
            'data': requestData,
            'success': function(data) {
                if (data.success) {
                    console.log('Saved!');
                } else {
                    console.error(data.errors);
                }
            },
            'error': function(){
                console.error('Something wrong!');
            }
        });
    };

    $(document).ready(function(){
        var $sortableTable = $('.sortable');
        if ($sortableTable.length > 0) {
            $sortableTable.sortable({
                handle: '.sortable-handle',
                axis: 'y',
                update: function(a, b){

                    var entityName = $(this).data('entityname');
                    var $sorted = b.item;

                    var $previous = $sorted.prev();
                    var $next = $sorted.next();

                    if ($previous.length > 0) {
                        changePosition({
                            parentId: $sorted.data('parentid'),
                            type: 'moveAfter',
                            entityName: entityName,
                            id: $sorted.data('itemid'),
                            positionEntityId: $previous.data('itemid')
                        });
                    } else if ($next.length > 0) {
                        changePosition({
                            parentId: $sorted.data('parentid'),
                            type: 'moveBefore',
                            entityName: entityName,
                            id: $sorted.data('itemid'),
                            positionEntityId: $next.data('itemid')
                        });
                    } else {
                        console.error('Something wrong!');
                    }
                },
                cursor: "move"
            });
        }
    });
</script>
@endPush
