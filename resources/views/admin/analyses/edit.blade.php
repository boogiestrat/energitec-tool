@extends('layouts.app', ['activePage' => 'analyses', 'titlePage' => __('Analyses')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Édition de l'analyse <strong>{{ $analyse->titre }}</strong></div>
                <div class="card-body">
                    <form action="{{ route('admin.analyses.update', $analyse) }}" method="post">
                        @csrf
                        @method('put')

                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input type="text" class="form-control" id="nom" name="nom" value="{{ old('nom', $analyse->nom) }}">
                        </div>
                        <div class="form-group">
                            <label for="titre">Titre</label>
                            <input type="text" class="form-control" id="titre" name="titre" value="{{ old('titre', $analyse->titre) }}">
                        </div>
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select id="type" name="type" class="form-control">
                                <option value="text" {{ $analyse->type == 'text' ? 'checked="checked"' : '' }}>Texte / numérique</option>
                                <option value="checkbox" {{ $analyse->type == 'checkbox' ? 'checked="checked"' : '' }}>Check Box</option>
                                <option value="select" {{ $analyse->type == 'select' ? 'checked="checked"' : '' }}>Dropdown</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="equipement_id">Équipement</label>
                            <select id="equipement_id" name="equipement_id" class="form-control">
                                <option value="0">Tous les équipements</option>
                                @foreach($equipements as $equipement)
                                    <option value="{{ $equipement->id }}" {{ $equipement->id == $analyse->equipement_id ? 'checked="checked"' : '' }}>{{ $equipement->nom }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="groupe">Groupe</label>
                            <select id="groupe" name="groupe" class="form-control">
                                <option value="materiel" {{ $analyse->groupe == 'materiel' ? 'checked="checked"' : '' }}>Matériel</option>
                                <option value="chimique" {{ $analyse->groupe == 'chimique' ? 'checked="checked"' : '' }}>Produit chimique</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
