@extends('layouts.app', ['activePage' => 'analyses', 'titlePage' => __('Analyses')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nouvelle analyse</div>
                <div class="card-body">
                    <form action="{{ route('admin.analyses.store') }}" method="POST">
                        @csrf
                        @method('post')
                        <div class="form-group">
                            <label for="input-nom">Nom</label>
                            <input type="text" class="form-control" id="input-nom" name="nom">
                        </div>
                        <div class="form-group">
                            <label for="input-titre">Titre</label>
                            <input type="text" class="form-control" id="input-titre" name="titre">
                        </div>
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select id="type" class="form-control" name="type">
                                <option value="text">Texte / numérique</option>
                                <option value="checkbox">Check Box</option>
                                <option value="select">Dropdown</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="equipement_id">Équipement</label>
                            <select id="equipement_id" class="form-control" name="equipement_id">
                                <option value="0" selected="selected">Tous les équipements</option>
                                @foreach($equipements as $equipement)
                                    <option value="{{ $equipement->id }}">{{ $equipement->nom }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="groupe">Groupe</label>
                            <select id="groupe" class="form-control" nom="groupe">
                                <option value="materiel" selected="selected">Matériel</option>
                                <option value="chimique">Produit chimique</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
