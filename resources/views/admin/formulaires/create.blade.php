@extends('layouts.app', ['activePage' => 'formulaires', 'titlePage' => __('Formulaire')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Ajouter un équipement pour {{ $user->compagnie }}</div>
                <div class="card-body">
                    <form action="{{ route('admin.formulaires.store', $user) }}" method="post">
                        @csrf
                        @method('post')
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="equipement_id">Équipement</label>
                                <select id="equipement_id" class="form-control" name="equipement_id">
                                    @foreach($equipements as $equipement)
                                        <option value="{{ $equipement->id }}">{{ $equipement->nom }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="equipement_titre">Nom de l'équipement (si différent)</label>
                                <input type="text" id="equipement_titre" class="form-control" name="equipement_titre">
                            </div>
                        </div>
                        <hr>
                        <h3>Analyses</h3>

                        @foreach($analyses as $analyse)
                        <div class="form-row analyse">
                            <div class="form-group col-md-3">
                                <input type="checkbox" name="is_active_{{ $analyse->id }}" value="1"> &nbsp;
                                <label for="is_active_{{ $analyse->id }}">{{ $analyse->titre }}</label>
                            </div>
                            @if($analyse->type == 'text' && $analyse->groupe == 'materiel')
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="unite_{{ $analyse->id }}" name="unite_{{ $analyse->id }}" placeholder="Unité">
                            </div>
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="min_{{ $analyse->id }}" name="min_{{ $analyse->id }}" placeholder="Minimum">
                            </div>
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="max_{{ $analyse->id }}" name="max_{{ $analyse->id }}" placeholder="Maximum">
                            </div>
                        </div>
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control champ-jaune" id="jaune_min_{{ $analyse->id }}" name="jaune_min_{{ $analyse->id }}" placeholder="Minimum">
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control champ-jaune" id="jaune_max_{{ $analyse->id }}" name="jaune_max_{{ $analyse->id }}" placeholder="Maximum">
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control champ-orange" id="orange_min_{{ $analyse->id }}" name="orange_min_{{ $analyse->id }}" placeholder="Minimum">
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control champ-orange" id="orange_max_{{ $analyse->id }}" name="orange_max_{{ $analyse->id }}" placeholder="Maximum">
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control champ-rouge" id="rouge_min_{{ $analyse->id }}" name="rouge_min_{{ $analyse->id }}" placeholder="Minimum">
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control champ-rouge" id="rouge_max_{{ $analyse->id }}" name="rouge_max_{{ $analyse->id }}" placeholder="Maximum">
                                </div>
                            @endif
                        </div>
                        @endforeach
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
