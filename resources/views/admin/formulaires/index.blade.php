@extends('layouts.app', ['activePage' => 'formulaires', 'titlePage' => __('Formulaires')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Formulaires <a href="{{ route('admin.formulaires.create', $user) }}" class="btn btn-link float-right"><i class="fa fa-plus"></i></a></div>

                <div class="card-body">
                    <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Compagnie</th>
                                <th scope="col">Équipement</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($formulaires as $formulaire)
                              <tr>
                                <th scope="row">{{ $formulaire->id }}</th>
                                <td>{{ $formulaire->client->compagnie }}</td>
                                <td>{{ $formulaire->equipement->nom }}</td>
                                <td>
                                    <a href="{{ route('admin.formulaires.edit', $formulaire) }}"><button type="button" class="btn btn-link float-left"><i class="fa fa-pencil"></i></button></a>
                                    <form action="{{ route('admin.formulaires.destroy', $formulaire) }}" method="POST" class="float-left">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button type="button" class="btn btn-link"><i class="fa fa-trash-o"></i></button>
                                    </form>
                                </td>
                              </tr>
                            @endForeach
                            </tbody>
                          </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
