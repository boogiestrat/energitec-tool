@extends('layouts.app', ['activePage' => 'equipements', 'titlePage' => __('equipements')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Équipements <a href="{{ route('admin.equipements.create') }}" class="btn btn-primary float-right">Ajouter</a></div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nom</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($equipements as $equipement)
                            <tr>
                                <th scope="row">{{ $equipement->nom }}</th>
                                <td>
                                    <a href="{{ route('admin.equipements.edit', $equipement->id) }}"><button type="button" class="btn btn-link float-left"><i class="fa fa-pencil"></i></button></a>
                                    <form action="{{ route('admin.equipements.destroy', $equipement) }}" method="POST" class="float-left">
                                        @csrf
                                        @method('delete')
                                        <button type="button" class="btn btn-link" onclick="confirm('{{ __("Êtes-vous certain de vouloir supprimer cette equipement?") }}') ? this.parentElement.submit() : ''"><i class="fa fa-trash-o"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endForeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
