@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Users')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Édition de l'utilisateur {{ $user->name }}</div>

                <div class="card-body">
                    <form action="{{ route('user.update', $user) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <h3>Détails</h3>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Nom</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Courriel</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="password">Mot de passe</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="confirm-password">Confirmer le mot de passe</label>
                                <input type="password" class="form-control" id="confirm-password" name="confirm-password">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-9">
                                <label for="avatarFile">Photo</label>
                                <input type="file" class="form-control-file" name="avatarFile" id="avatarFile">
                            </div>
                            <div class="form-group col-md-3">
                                <img src="/assets/images/avatars/{{ $user->avatar }}" class="user-photo" style="max-height:40px;border:solid 1px #333;">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Compagnie: </label>
                                {{ $user->compagnie }}
                            </div>
                            <div class="form-group col-md-6">
                                <label for="poste">Poste</label>
                                <input type="text" class="form-control" id="poste" name="poste" value="{{ $user->poste }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="adresse">Adresse</label>
                                <input type="text" class="form-control" id="adresse" name="adresse" value="{{ $user->adresse }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ville">Ville</label>
                                <input type="text" class="form-control" id="ville" name="ville" value="{{ $user->ville }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="province">Province</label>
                                <select id="province" name="province" class="form-control">
                                    <option value="QC">QC</option>
                                    <option value="ON">ON</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="code_postal">Code postal</label>
                                <input type="text" class="form-control" id="code_postal" name="code_postal" value="{{ $user->code_postal }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="telephone">Téléphone</label>
                                <input type="text" class="form-control" id="telephone" name="telephone" value="{{ $user->telephone }}">
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-primary">Sauvegarder</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
