@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Users')])

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">clients</div>

                <div class="card-body">
                    <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Compagnie</th>
                                <th scope="col">Rapports</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                              <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->compagnie }}</td>
                                <td><a  href="{{ route('rapports.create', $user) }}"><button type="button" class="btn btn-link"><i class="fa fa-plus"></i></button></a>
                                </td>
                              </tr>
                            @endForeach
                            </tbody>
                          </table>
                </div>
            </div>
        </div>
    </div>

@endsection
