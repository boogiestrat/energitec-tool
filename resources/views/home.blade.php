@extends('layouts.app', ['activePage' => 'home', 'titlePage' => __('Dashboard')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Gestion des rapports</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenue {{ auth()->user()->name }}
                    <ul style="list-style: none;margin: 1rem 0;padding-left:0;">
                    @if(auth()->user()->hasRole('operateur'))
                    
                        <li><a href="/rapports"><i class="icon-pie-chart"></i> <span> Rapports</span></a></li>
                        
                    
                    @endif
                    @if(auth()->user()->hasRole('client') || auth()->user()->hasRole('operateur-client'))
                        <li><a href="/stats/{{ auth()->user()->id }}"><i class="icon-bar-chart"></i> <span> Statistiques</span></a></li>
                    @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
