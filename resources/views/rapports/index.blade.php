@extends('layouts.app', ['activePage' => 'rapports', 'titlePage' => __('Rapports')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Rapports</div>

                <div class="card-body">
                    <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">Entreprise</th>
                                <th scope="col">Rapports</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $client)
                              <tr>
                                <th scope="row">{{ $client->compagnie }}</th>
                                <td>
                                    <a href="{{ route('rapports.list', $client) }}">Rapports</a>
                                </td>
                              </tr>
                            @endForeach
                            </tbody>
                          </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
