@extends('layouts.app', ['activePage' => 'rapport', 'titlePage' => __('Rapport')])

@section('content')
      <div class="row">
        <div class="col-md-12">
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Rapport pour {{ $rapport->client->compagnie }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-6">{{ \Carbon\Carbon::parse($rapport->updated_at)->format('d/m/Y') }}</div>
                  <div class="col-6 text-right" style="margin-bottom:1rem;">
                    <a href="{{ route('rapports.export', $rapport) }}" class="btn btn-sm btn-primary"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="{{ route('rapports.list', $rapport->client) }}" class="btn btn-sm btn-primary">Retour</a>
                  </div>
                </div>
                <div class="row">
                    <div class="col" style="overflow: auto;padding:0;">
                        <table class="table table-striped table-bordered table-responsive-sm table-rapport">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Analyse</th>
                                    @foreach($rapport->client->formulaires as $formulaire)
                                        <th scope="col" class="text-center">{!! $formulaire->equipement_titre !!}</th>
                                    @endForeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($analyses as $analyse)
                                    @if($rapport->client->hasAnyAnalyse($analyse->id))
                                        <tr>
                                            <th scope="row">{{ $analyse->titre }}</th>
                                        @foreach($rapport->client->formulaires as $formulaire)
                                            <td>
                                                @if($formulaire->HasChamp($analyse->id))
                                                    @if(isset($contenu->{$analyse->nom.'_'.$formulaire->id}) && $contenu->{$analyse->nom.'_'.$formulaire->id} == '1')
                                                    <i class="fa fa-check vert"></i>
                                                    @elseif($analyse->nom != 'durete' && $analyse->nom != 'purge_fond_secondes' && isset($contenu->{$analyse->nom.'_'.$formulaire->id}) && $contenu->{$analyse->nom.'_'.$formulaire->id} == '0')
                                                    <i class="fa fa-ban"></i>
                                                    @else
                                                    <span class="{{ $contenu->{$analyse->nom.'_'.$formulaire->id.'_couleur'} ?? '' }}">
                                                    {{ $contenu->{$analyse->nom.'_'.$formulaire->id} ?? '-' }} {{ $formulaire->getChamp($analyse->id)->unite }}
                                                    </span>
                                                    {!! $formulaire->getChamp($analyse->id)->min != '' || $formulaire->getChamp($analyse->id)->max != '' ? "<br><small>(" : '' !!}
                                                    {{ $formulaire->getChamp($analyse->id)->min }}
                                                        @if($formulaire->getChamp($analyse->id)->max == '' && $formulaire->getChamp($analyse->id)->min != '')
                                                            min
                                                        @endif

                                                        @if($formulaire->getChamp($analyse->id)->min != '' && $formulaire->getChamp($analyse->id)->max != '')
                                                                -
                                                        @endif

                                                        {{ $formulaire->getChamp($analyse->id)->max }}
                                                        @if($formulaire->getChamp($analyse->id)->min == '' && $formulaire->getChamp($analyse->id)->max != '')
                                                            max
                                                        @endif
                                                    {!! $formulaire->getChamp($analyse->id)->min != '' || $formulaire->getChamp($analyse->id)->max != '' ? ')</small>' : '' !!}
                                                    @endif
                                                    @if(isset($contenu->{$analyse->nom.'_'.$formulaire->id.'_comment'}))
                                                    <button type="button" class="btn btn-link" data-toggle="modal" data-target=".{{ $analyse->nom.'_'.$formulaire->id.'_comment' }}">
                                                            <i class="fa fa-exclamation-triangle"></i>
                                                    </button>
                                                    <div class="modal fade {{ $analyse->nom.'_'.$formulaire->id.'_comment' }}" tabindex="-1" role="dialog" aria-labelledby="{{ $analyse->nom.'_'.$formulaire->id.'_comment' }}Label" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title h4" id="{{ $analyse->nom.'_'.$formulaire->id.'_comment' }}Label">{{ $analyse->titre }}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Valeur hors-norme.</p>
                                                                        <p>{{ $contenu->{$analyse->nom.'_'.$formulaire->id.'_comment'} }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endif
                                            </td>
                                        @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        <p>{{ $rapport->notes ?? '' }}</p>
                    </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
@endsection
