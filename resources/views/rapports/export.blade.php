@extends('layouts.pdf', ['activePage' => 'rapport', 'titlePage' => __('Rapport')])

@section('content')

      <div class="row rapport-header">
        <div class="col-md-6 bb">
            <strong>Rapport du ({{ \Carbon\Carbon::parse($rapport->updated_at)->format('d/m/Y') }})</strong>
        </div>
        <div class="col-md-6 text-right bb">
            {{ $rapport->client->compagnie }}<br>
            {{ $rapport->client->adresse }}<br>
            {{ $rapport->client->ville }}, {{ $rapport->client->province }}
        </div>
      </div>
      @if(isset($rapport->notes))
      <div class="row rapport-notes">
          <div class="col">
            <h5>Recommendations et commentaires</h5>
            <p>{{ $rapport->notes ?? '' }}</p>
          </div>
      </div>
      <div class="page-break"></div>
      @endif
      @php ($x = 0)
      @foreach($formulaires as $equipment => $forms)
      @php (++$x)
      <div class="row">
        <div class="col-sm-12">
            <h5>{{ $equipment }}</h5>
        </div>
      </div>
      <div class="row rapport-content">
        <div class="col-sm-12">
            <div class="rapport-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Analyse</th>
                            @foreach($forms as $formulaire)
                                <th scope="col" class="text-center">
                                    {!! $formulaire->equipement_titre !!}
                                </th>
                            @endForeach
                        </tr>
                    </thead>
                    <tbody>
                        @php ($i = 1)
                        @foreach($analyses as $analyse)
                            @if($rapport->hasAnyAnalyse($analyse->id, $forms))
                            @php (++$i)
                                <tr {{ $i %2 == 0 ? 'style=background-color:#cfe0f3;' : '' }}>
                                    <th scope="row">{{ $analyse->titre }}</th>
                                @foreach($forms as $formulaire)
                                    <td>
                                        @if($formulaire->HasChamp($analyse->id))
                                            @if(isset($contenu->{$analyse->nom.'_'.$formulaire->id}) && $contenu->{$analyse->nom.'_'.$formulaire->id} == '1')
                                            <i class="fa fa-check vert"></i>
                                            @elseif($analyse->nom != 'durete' && $analyse->nom != 'purge_fond_secondes' && isset($contenu->{$analyse->nom.'_'.$formulaire->id}) && $contenu->{$analyse->nom.'_'.$formulaire->id} == '0')
                                            <i class="fa fa-ban rouge"></i>
                                            @else
                                            <span class="{{ $contenu->{$analyse->nom.'_'.$formulaire->id.'_couleur'} ?? ''}}">
                                            {{ $contenu->{$analyse->nom.'_'.$formulaire->id} ?? '-' }} {{ $formulaire->getChamp($analyse->id)->unite }}
                                            </span>
                                            {!! $formulaire->getChamp($analyse->id)->min != '' || $formulaire->getChamp($analyse->id)->max != '' ? "<br><small>(" : '' !!}
                                            {{ $formulaire->getChamp($analyse->id)->min }}
                                                @if($formulaire->getChamp($analyse->id)->max == '' && $formulaire->getChamp($analyse->id)->min != '')
                                                    min
                                                @endif

                                                @if($formulaire->getChamp($analyse->id)->min != '' && $formulaire->getChamp($analyse->id)->max != '')
                                                        -
                                                @endif

                                                {{ $formulaire->getChamp($analyse->id)->max }}
                                                @if($formulaire->getChamp($analyse->id)->min == '' && $formulaire->getChamp($analyse->id)->max != '')
                                                    max
                                                @endif
                                            {!! $formulaire->getChamp($analyse->id)->min != '' || $formulaire->getChamp($analyse->id)->max != '' ? ')</small>' : '' !!}
                                            @endif
                                            @if(isset($contenu->{$analyse->nom.'_'.$formulaire->id.'_comment'}))
                                                <i class="fa fa-exclamation-triangle"></i>
                                            @endif
                                        @endif
                                    </td>
                                @endforeach
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
            <h5>Commentaires pour {{ $equipment }}</h5>
        </div>
      </div>
      <div class="row rapport-content">
        <div class="col-sm-12">
            <div class="rapport-content">
                <div class="commentaires">
                    @foreach($analyses as $analyse)
                        @foreach($forms as $formulaire)
                            @if($rapport->hasAnyAnalyse($analyse->id, $forms) && isset($contenu->{$analyse->nom.'_'.$formulaire->id.'_comment'}))
                                <h6>
                                    <i class="fa fa-exclamation-triangle"></i>
                                    {!! $formulaire->equipement_titre !!}: {{ $analyse->titre }} 
                                    (valeur hors-norme: {{ $contenu->{$analyse->nom.'_'.$formulaire->id} ?? '-' }} 
                                    {{ $formulaire->getChamp($analyse->id)->unite }})
                                </h6>
                                <p>{{ $contenu->{$analyse->nom.'_'.$formulaire->id.'_comment'} }}</p>
                                <hr>  
                            @endif
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    </div> 
    <div class="page-break"></div>
    @endforeach

@endsection
