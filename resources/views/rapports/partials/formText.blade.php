@if($formulaire->hasChamp($champ) )
@php
  $min = $champ . '_min';
  $max = $champ . '_max';
@endphp
    <input type="text" id="{{ $champ }}-{{ $formulaire->id }}" class="form-control">
    @if(($formulaire->$min && $formulaire->$min != '') || ($formulaire->$max && $formulaire->$max != ''))
    <label>
        {{ $formulaire->$min }}
        @if($formulaire->$max == '')
            min
        @endif

        @if($formulaire->$min != '' && $formulaire->$max != '')
             -
        @endif

        {{ $formulaire->$max }}
        @if($formulaire->$min == '')
            max
        @endif
    </label>
    @endif
@endif
