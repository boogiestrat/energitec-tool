@if($client->anyFormulaireHasChamp($champ))
    <tr>
        <th scope="row">{{ $title }}</th>
        @foreach($client->formulaires as $formulaire)
            <td>
                @if($type == 'Checkbox')
                    @include('rapports.partials.formCheckbox', ['champ' => $champ])
                @else
                    @include('rapports.partials.formText', ['champ' => $champ])
                @endif
            </td>
        @endForeach
    </tr>
@endif
