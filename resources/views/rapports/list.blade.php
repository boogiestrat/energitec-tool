@extends('layouts.app', ['activePage' => 'rapports', 'titlePage' => __('Rapports')])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Rapports pour {{ $client->compagnie }} <a href="{{ route('rapports.create', $client) }}" class="btn btn-primary float-right">Ajouter</a></div>

                <div class="card-body">
                    <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">Rapport #</th>
                                <th scope="col">Opérateur</th>
                                <th scope="col">Date</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($rapports as $rapport)
                              <tr>
                                <th scope="row">{{ $rapport->id }}</th>
                                <td>{{ $rapport->operateur->name }}</td>
                                <td>{{ $rapport->updated_at }}</td>
                                <td>
                                    <a href="{{ route('rapports.show', $rapport) }}"><button type="button" class="btn btn-link"><i class="fa fa-eye"></i></button></a>
                                    <a href="{{ route('rapports.export', $rapport) }}"><button type="button" class="btn btn-link"><i class="fa fa-file-pdf-o"></i></button></a>
                                    @if( $rapport->incomplet == '1' || auth()->user()->hasRole('admin'))
                                    <a href="{{ route('rapports.edit', $rapport) }}"><button type="button" class="btn btn-link"><i class="fa fa-pencil"></i></button></a>
                                    @endif
                                </td>
                              </tr>
                            @endForeach
                            </tbody>
                          </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
