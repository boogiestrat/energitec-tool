@extends('layouts.app', ['activePage' => 'rapport-management', 'titlePage' => __('Rapports')])

@section('content')
  <div class="content">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('rapports.store', $client) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Créer un rapport</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                    @if(auth()->user()->hasRole('admin'))
                      <a href="{{ route('rapports.index') }}" class="btn btn-sm btn-primary">Retour à la liste</a>
                      @else
                      <a href="{{ route('admin.users.show', $client) }}" class="btn btn-sm btn-primary">Retour à la liste</a>
                      @endif
                  </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Entreprise</label>
                            <input type="hidden" id="operateur_id" value="{{ $operateur->id }}">
                            {{ $client->compagnie }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="overflow: auto;padding:0;">
                        <table class="table table-striped table-bordered table-responsive-sm table-rapport">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Analyse</th>
                                    @foreach($client->formulaires as $formulaire)
                                        <th scope="col" class="text-center">
                                            {!! $formulaire->equipement_titre !!}
                                        </th>
                                    @endForeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($analyses as $analyse)
                                    @if($client->hasAnyAnalyse($analyse->id))
                                        <tr>
                                            <th scope="row">{{ $analyse->titre }}</th>
                                        @foreach($client->formulaires as $formulaire)
                                            <td style="text-align:center">
                                                @if($formulaire->HasChamp($analyse->id))
                                                    @if($analyse->type =='checkbox')
                                                        <input type='hidden' value='0' name="{{ $analyse->nom }}_{{ $formulaire->id }}">
                                                        <input type="checkbox" id="{{ $analyse->nom }}_{{ $formulaire->id }}" name="{{ $analyse->nom }}_{{ $formulaire->id }}" value="1">
                                                    @elseif($analyse->type =='select')
                                                        <select id="{{ $analyse->nom }}_{{ $formulaire->id }}" name="{{ $analyse->nom }}_{{ $formulaire->id }}">
                                                            <option value="on">On</option>
                                                            <option value="off">Off</option>
                                                            <option value="standby">Stand-By</option>
                                                        </select>
                                                    @else
                                                        <input type="text" class="form-control" id="{{ $analyse->nom }}_{{ $formulaire->id }}" name="{{ $analyse->nom }}_{{ $formulaire->id }}" style="max-width:90%;text-align:center;" placeholder="{{ $formulaire->getChamp($analyse->id)->unite ?? '' }}">
                                                        <label for="{{ $analyse->nom }}_{{ $formulaire->id }}">
                                                                {{ $formulaire->getChamp($analyse->id)->min }}
                                                                @if($formulaire->getChamp($analyse->id)->max == '' && $formulaire->getChamp($analyse->id)->min != '')
                                                                    min
                                                                @endif

                                                                @if($formulaire->getChamp($analyse->id)->min != '' && $formulaire->getChamp($analyse->id)->max != '')
                                                                     -
                                                                @endif

                                                                {{ $formulaire->getChamp($analyse->id)->max }}
                                                                @if($formulaire->getChamp($analyse->id)->min == '' && $formulaire->getChamp($analyse->id)->max != '')
                                                                    max
                                                                @endif
                                                        </label>
                                                        <button type="button" class="btn btn-link" data-toggle="modal" id="{{ $analyse->nom.'_'.$formulaire->id.'_comment_trigger' }}" data-target="#{{ $analyse->nom.'_'.$formulaire->id.'_comment_modal' }}">
                                                                <i class="fa fa-exclamation-triangle"></i>
                                                        </button>
                                                        <div class="modal fade" id="{{ $analyse->nom.'_'.$formulaire->id.'_comment_modal' }}" tabindex="-1" role="dialog" aria-labelledby="{{ $analyse->nom.'_'.$formulaire->id.'_comment_modal' }}Label" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title h4" id="{{ $analyse->nom.'_'.$formulaire->id.'_comment' }}Label">Valeur hors-norme</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p class="text-left">Le dernier rapport affichait {{ $precedent->{$analyse->nom.'_'.$formulaire->id} ?? 'rien' }}</p>
                                                                        <p class="text-left">Décrivez la raison:</p>
                                                                        <textarea class="form-control" id="{{ $analyse->nom }}_{{ $formulaire->id }}_comment" name="{{ $analyse->nom }}_{{ $formulaire->id }}_comment">{{ $precedent->{$analyse->nom.'_'.$formulaire->id.'_comment'} ?? '' }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @push('scripts')
                                                        <script>
                                                            $( "#{{ $analyse->nom }}_{{ $formulaire->id }}" ).blur(function() {
                                                                let couleur = 'jaune';
                                                                let enteredValue = this.value;
                                                                let min = {{ $formulaire->getChamp($analyse->id)->min != '' ? $formulaire->getChamp($analyse->id)->min : 0 }};
                                                                let max = {{ $formulaire->getChamp($analyse->id)->max != '' ? $formulaire->getChamp($analyse->id)->max : 0 }};
                                                                let jauneMin = {{ $formulaire->getChamp($analyse->id)->jaune_min != '' ? $formulaire->getChamp($analyse->id)->jaune_min : 0 }};
                                                                let jauneMax = {{ $formulaire->getChamp($analyse->id)->jaune_max != '' ? $formulaire->getChamp($analyse->id)->jaune_max : 0 }};
                                                                let orangeMin = {{ $formulaire->getChamp($analyse->id)->orange_min != '' ? $formulaire->getChamp($analyse->id)->orange_min : 0 }};
                                                                let orangeMax = {{ $formulaire->getChamp($analyse->id)->orange_max != '' ? $formulaire->getChamp($analyse->id)->orange_max : 0 }};

                                                                if( max == 0 && min != 0) {
                                                                    if( enteredValue < min ) {
                                                                        @if('' != $formulaire->getChamp($analyse->id)->jaune_min)
                                                                        if(enteredValue < jauneMin) {
                                                                            couleur = 'orange';
                                                                        }
                                                                        @endif
                                                                        @if('' != $formulaire->getChamp($analyse->id)->orange_min)
                                                                        if(enteredValue < orangeMin) {
                                                                            couleur = 'rouge';
                                                                        }
                                                                        @endif
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge').addClass(couleur);
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment_trigger' }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge').addClass(couleur);
                                                                    }
                                                                    else {
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge');
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment_trigger' }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge');
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment' }}").val('');
                                                                    }
                                                                }

                                                                if( min != 0 && max != 0) {
                                                                    if( enteredValue < min || enteredValue > max ) {
                                                                        @if('' != $formulaire->getChamp($analyse->id)->jaune_min || '' != $formulaire->getChamp($analyse->id)->jaune_max )
                                                                        if(enteredValue < jauneMin || enteredValue  > jauneMax) {
                                                                            couleur = 'orange';
                                                                        }
                                                                        @endif
                                                                        @if('' != $formulaire->getChamp($analyse->id)->orange_min || '' != $formulaire->getChamp($analyse->id)->orange_max )
                                                                        if(enteredValue < orangeMin || enteredValue > orangeMax) {
                                                                            couleur = 'rouge';
                                                                        }
                                                                        @endif
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge').addClass(couleur);
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment_trigger' }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge').addClass(couleur);
                                                                    }
                                                                    else {
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge');
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment_trigger' }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge');
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment' }}").val('');
                                                                    }
                                                                }

                                                                if( min == 0 && max != 0 ) {
                                                                    if( enteredValue > max ) {
                                                                        @if('' != $formulaire->getChamp($analyse->id)->orange_max)
                                                                        if(enteredValue  > jauneMax) {
                                                                            couleur = 'orange';
                                                                        }
                                                                        @endif
                                                                        @if('' != $formulaire->getChamp($analyse->id)->orange_max)
                                                                        if(enteredValue > orangeMax) {
                                                                            couleur = 'rouge';
                                                                        }
                                                                        @endif
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge').addClass(couleur);
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment_trigger' }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge').addClass(couleur);
                                                                    }
                                                                    else {
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge');
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment_trigger' }}" ).removeClass('jaune').removeClass('orange').removeClass('rouge');
                                                                        $( "#{{ $analyse->nom.'_'.$formulaire->id.'_comment' }}").val('');
                                                                    }
                                                                }
                                                            });
                                                        </script>
                                                        @endpush
                                                    @endif
                                                @endif
                                            </td>
                                        @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="" name="incomplet" value="1">
                                <label class="form-check-label">Rapport non complété</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="advise_admin" name="advise_admin" value="1">
                                <label class="form-check-label">Envoyer à l'administration</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="notes">Notes additionnelles</label>
                            <textarea class="form-control" id="notes" name="notes" rows="4"></textarea>
                        </div>
                    </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">Sauvegarder</button>
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
@endsection
