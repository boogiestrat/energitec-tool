<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<title>Energitec</title>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{ config('app.url') }}/assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}/assets/vendor/font-awesome/css/font-awesome.min.css">
<!-- MAIN CSS -->

<style>
.content {font-size:12px;}
.page-break {page-break-after: always;}
div.bb {padding-top:5px;padding-bottom:5px;border-top-style: solid;border-top-width: 1px;border-top-color: #A5C3E0;}
h5 {background-color: #597288;color: #fff;padding: 3px 10px 4px;margin-top: 1rem;margin-bottom: 1rem;margin-top:0;font-size: 1rem;}
table.table {width: auto;font-size:12px;}
.table-bordered {border:none;}
.table tr th {color:#fff;background-color:#9a9a9a;min-width:160px;max-width:180px;padding:8px;}
.table tr td {min-width:160px;text-align:center;background:none;min-width:160px;max-width:160px;padding:8px;}
.jaune {color: #ffc107;}
.vert {color:green;}
.rouge {color:red;}
.logo {max-height:45px;display:block;margin:10px auto;}
.table i {margin: 0 auto;display:block}
.rapport-content {display: flex;flex-wrap: wrap;}
.rapport-content small {font-size:.7rem}
.rapport-content i.fa-exclamation-triangle {display:inline-block;float:right;margin:0 5px;color:#ffa117;}
div.commentaires {color:#333;}
div.commentaires h6 {font-weight:bold;background-color:#9a9a9a;color:#fff;padding:5px;}
</style>
</head>
<body>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="{{ config('app.url') }}/assets/images/logo-sm.png" alt="Energitec" class="img-fluid logo">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" style="color:#007bff;margin-bottom:10px;">
                    <h3 style="font-weight:bold;">Surveillance Périodique Energitec Inc.</h3>
                    Mécanicien de Machines Fixes, Surveillance Périodique, Équipements de traitement d&#39;eau
                </div>
            </div>

            @yield('content')

        </div>
    </div>
</body>
</html>
