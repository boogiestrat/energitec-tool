<div id="left-sidebar" class="sidebar">
        <div class="navbar-brand">
            <a href="/"><img src="/assets/images/logo-sm.png" alt="Energitec" class="img-fluid logo"><span>Energitec</span></a>
            <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
        </div>
        <div class="sidebar-scroll">
            <div class="user-account">
                <div class="user_div">
                    <img src="/assets/images/avatars/{{ auth()->user()->avatar }}" class="user-photo" alt="{{ auth()->user()->name }}">
                </div>
                <div class="dropdown">
                    <span>Bienvenue,</span>
                    <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>{{ auth()->user()->name }}</strong></a>
                    <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">
                        <li><a href="{{ route('user.edit', auth()->user()) }}"><i class="icon-user"></i>Mon profil</a></li>
                        <!--<li><a href="#"><i class="icon-envelope-open"></i>Messages</a></li>-->
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-power"></i>Déconnexion</a></li>
                    </ul>
                </div>
            </div>
            <nav id="left-sidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="header">Outils</li>
                    <li><a href="/home"><i class="icon-home"></i><span>Accueil</span></a></li>
                    @if(auth()->user()->hasRole('admin'))
                    <li class="header">Admin</li>
                    <li><a href="/admin/users/list/client"><i class="icon-users"></i><span>Clients</span></a></li>
                    <li><a href="/admin/users/list/operateur"><i class="icon-users"></i><span>Opérateurs</span></a></li>
                    <li><a href="/admin/users/list/admin"><i class="icon-users"></i><span>Administrateurs</span></a></li>
                    <li><a href="/admin/equipements"><i class="icon-puzzle"></i><span>Équipements</span></a></li>
                    <li><a href="/admin/analyses"><i class="icon-chemistry"></i><span>Analyses</span></a></li>
                    @elseif(auth()->user()->hasRole('operateur'))
                    <li><a href="/rapports"><i class="icon-pie-chart"></i><span>Rapports</span></a></li>
                    @else
                    <li><a href="{{ route('rapports.list', auth()->user()->companyId()) }}"><i class="icon-pie-chart"></i><span>Rapports</span></a></li>
                    @endif
                    <li class="header">Autre</li>
                    <li><a href="/documentation"><i class="icon-doc"></i><span>Documentation</span></a></li>
                </ul>
            </nav>
        </div>
    </div>
