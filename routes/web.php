<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/users/list', ['as' => 'users.list', 'uses' => 'UserController@list']);
    Route::get('/user/{user}/edit', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
    Route::post('/user/{user}/update', ['as' => 'user.update', 'uses' => 'UserController@update']);
    Route::get('/rapports', ['as' => 'rapports.index', 'uses' => 'RapportsController@index']);
    Route::get('/rapports/{rapport}', ['as' => 'rapports.show', 'uses' => 'RapportsController@show']);
    Route::get('/rapports/{rapport}/export', ['as' => 'rapports.export', 'uses' => 'RapportsController@export']);
    Route::get('/rapports/{rapport}/edit', ['as' => 'rapports.edit', 'uses' => 'RapportsController@edit']);
    Route::post('/rapports/{rapport}/{user}/update', ['as' => 'rapports.update', 'uses' => 'RapportsController@update']);
    Route::get('/rapports/{client}/list', ['as' => 'rapports.list', 'uses' => 'RapportsController@list']);
    Route::get('/rapports/{user}/create', ['as' => 'rapports.create', 'uses' => 'RapportsController@create']);
    Route::post('/rapports/{user}/store', ['as' => 'rapports.store', 'uses' => 'RapportsController@store']);
    Route::get('/stats/{client}/{range?}', ['as' => 'stats', 'uses' => 'HomeController@stats']);
});

Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function(){
    Route::resource('/users', 'UsersController', ['except' => ['create', 'store']]);
    Route::get('/users/{type}/create', ['as' => 'users.create', 'uses' => 'UsersController@create']);
    Route::post('/users/{type}/store', ['as' => 'users.store', 'uses' => 'UsersController@store']);
    Route::post('/users/{user}/{type}/destroy', ['as' => 'users.destroy', 'uses' => 'UsersController@destroy']);
    Route::get('/users/list/{type}', ['as' => 'users.list', 'uses' => 'UsersController@list']);
    Route::resource('/formulaires','FormulairesController', ['except' => ['show', 'create', 'store']]);
    Route::get('/formulaires/{user}/create/', ['as' => 'formulaires.create', 'uses' => 'FormulairesController@create']);
    Route::post('/formulaires/{user}/store/', ['as' => 'formulaires.store', 'uses' => 'FormulairesController@store']);
    Route::resource('/analyses','AnalysesController', ['except' => ['show']]);
    Route::post('/analyses/sort', '\Rutorika\Sortable\SortableController@sort');
    Route::resource('/equipements','EquipementsController', ['except' => ['show']]);
});
